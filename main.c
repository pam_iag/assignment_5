#include <stdio.h>
#include <stdlib.h>

#define pcost 500
#define attecost 3

double profit(double);
double revenue(double);
double cost(double);
int attendees(double);


int main()
{
    for (double price=5; price<=50; price+=5)
    {
        printf("The ticket price = Rs. %.2f\n The profit gained: Rs. %.2f\n",price,profit(price));
    }

}

double profit(double price)
{
    double profit = revenue(price)-cost(price);
}

double revenue(double price)
{
    return attendees(price)*price;
}

double cost(double price)
{
    return pcost+(attendees(price)*attecost);
}

int attendees(double price)
{
        return 120-(((price-15)/5)*20);
}
